package com.mysite.examination.mapper;

import java.util.List;
import java.util.Map;

import com.mysite.examination.model.SysGroup;

public interface SysGroupMapper {

	List<SysGroup> getGroups(Map<String, Object> searchMap);
}