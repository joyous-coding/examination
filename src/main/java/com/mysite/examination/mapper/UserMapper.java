package com.mysite.examination.mapper;

import java.util.List;
import java.util.Map;

import com.mysite.examination.model.User;

public interface UserMapper {

	int checkUser(User u);

	Integer getUsersCount(Map<String, Object> map);

	List<User> getUsers(Map<String, Object> map);

}
