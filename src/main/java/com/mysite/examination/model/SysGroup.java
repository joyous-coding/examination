package com.mysite.examination.model;

import java.io.Serializable;

public class SysGroup implements Serializable {

	transient private static final long serialVersionUID = -1L;

	private Integer id;

	private String groupName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
}