package com.mysite.examination.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysite.examination.model.SysGroup;
import com.mysite.examination.service.GroupService;

@Controller
@RequestMapping("/group")
public class GroupController {

	@Autowired
	private GroupService groupService;

	@RequestMapping(value = "", method = { RequestMethod.GET })
	public String index() {
		return "group";
	}

	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public List<SysGroup> getGroups(HttpServletRequest request) {
		Map<String, Object> searchMap = new HashMap<String, Object>();
		List<SysGroup> list = groupService.getGroups(searchMap);
		return list;
	}
}
