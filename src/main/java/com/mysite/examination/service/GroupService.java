package com.mysite.examination.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysite.examination.mapper.SysGroupMapper;
import com.mysite.examination.model.SysGroup;

@Service
public class GroupService {

	@Autowired
	private SysGroupMapper groupMapper;

	public List<SysGroup> getGroups(Map<String, Object> searchMap) {
		return groupMapper.getGroups(searchMap);
	}
}
