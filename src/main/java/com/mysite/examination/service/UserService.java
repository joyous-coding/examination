package com.mysite.examination.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysite.examination.mapper.UserMapper;
import com.mysite.examination.model.User;

@Service
public class UserService {
	@Autowired
	private UserMapper mapper;

	public  boolean doLogin(User u) {

		int count = mapper.checkUser(u);
		return count != 0;
	}

	public Map<String, Object> getUsers(Map<String, Object> map) {
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		
		Integer count = mapper.getUsersCount(map);
		
		List<User> list = mapper.getUsers(map);
		
		
		
		resultMap.put("count", count);
		
		resultMap.put("data", list);
		
		return resultMap;
	}

}
