CREATE DATABASE IF NOT EXISTS examination
  DEFAULT CHARSET utf8
  COLLATE utf8_general_ci;

USE examination;

DROP TABLE IF EXISTS `sys_group`;
CREATE TABLE `sys_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_name` varchar(255) DEFAULT NULL COMMENT '分组名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统分组表';
INSERT INTO `sys_group` VALUES ('1', '管理员用户组');
INSERT INTO `sys_group` VALUES ('2', '普通用户组');

DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `email` varchar(255) DEFAULT NULL COMMENT 'email',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '用户状态：1 有效； 0 无效',
  `update_time` datetime NOT NULL COMMENT '用户数据的修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统用户表';
INSERT INTO `sys_user` VALUES ('1', 'admin', '123456', 'admin@jjc12320.cn', '1', '2018-02-02 09:48:10');
