<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript">
$(function(){
	$.ajax({
		type : "get",
		url : "./group/list",
		dataType : "json",
		data : { },
		success : function(result) {
			console.log(result);
			$('#tb').empty();
			$.each(result, function(i, d){
				$('ul').append('<li>'+d.groupName+'</li>');
			});
		}
	});
});
</script>
<title>用户组</title>
</head>
<body>
<h1>这是用户组列表页；下面是用户组列表：</h1>
<ul></ul>
</body>
</html>