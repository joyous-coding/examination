<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>拓维云计算平台</title>
<link href="../css/base.css" rel="stylesheet" type="text/css" />
<link href="../css/second.css" rel="stylesheet" type="text/css" />
<link href="../css/input_style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/dialog.js"></script>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script>
	
$(function(){

	$("#login1").click(function(){
		$.ajax({
			type:"POST",
			url:"index",
			dataType:"json",
			data:{
				username:$("#username").val(),
				password:$("#password").val(),
			},
			success:function(result){
				console.log(result);
				if(result=="ok"){
					location.href="main"
				}else{
					alert('账号或者密码错误')
				}
			}		
		});
	})
	
});
</script> 

</head>

<body>
	<div class="logon">
		<div class="logon_poto"></div>
		<div class="logon_logo"></div>
		<div class="logon_list">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="49%" height="50">
						<input type="text" class="othfld" id="username" style="width: 220px;" placeholder="用户名" />
					</td>
					<td width="51%" height="50" class="font_2"></td>
				</tr>
				<tr>
					<td height="50">
						<input type="password" class="othfld" style="width: 220px;" id="password" placeholder="密码" />
					</td>
					<td height="50"><span class="font_2" style="display: none">用户名或密码错误</span></td>
				</tr>
				<tr>
					<td height="50">
						<a href="javascript:;"><img src="../images/button17.jpg" width="97" height="34"  id="login1"/></a>
					</td>
					<td height="50">&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
	<div id="footer">
		<div class="footer_about">
			<p>
				<a href="#">帮助</a>|<a href="#">意见反馈</a>|<a href="#">管理中心</a>
			</p>
			<p>Copyright © 1996-2013 All Right Reserved</p>
		</div>
	</div>
</body>
</html>
