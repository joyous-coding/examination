<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理员_用户</title>

<link href="../css/base.css" rel="stylesheet" type="text/css" />
<link href="../css/input_style.css" rel="stylesheet" type="text/css" />
<link href="../css/second.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/dialog.js"></script>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script type="text/javascript">
	function showUserData(pageIndex) {

		if (pageIndex == 'prev') {

			pageIndex = parseInt($("#currentPage").html()) - 1;

			if (pageIndex < 1)
				pageIndex = 1;

		} else if (pageIndex == 'next') {

			pageIndex = parseInt($("#currentPage").html()) + 1;

			if (parseInt($("#totalPage").html()) < pageIndex)
				pageIndex = parseInt($("#totalPage").html());

		} else if (pageIndex == 'last') {

			pageIndex = parseInt($("#totalPage").html());

		}

		var jsonData = {
			searchText : $("#searchText").val(),
			start : (pageIndex - 1) * 10,
			length : 10
		};

		$('#userList tbody').empty();

		$.ajax({
					type : "get",
					url : "users",
					dataType : "json",
					data : jsonData,
					success : function(result) {
						console.log(result);
						var count = result.count;
						var list = result.data;

						for (var i = 0; i < list.length; i++) {
							var str = '';
							str = str + '<tr>';
							str = str
									+ '<td height="36"><input type="checkbox" /></td>';
							str = str + '<td>' + list[i].username + '</td>';
							str = str + '<td>' + list[i].email + '</td>';
							str = str + '<td>'
									+ (list[i].status == 1 ? '正常' : '异常')
									+ '</td>';
							str = str
									+ '<td><a href="guangliyuan_yonhu_bianji.html" title="编辑">编辑</a>&nbsp;&nbsp;<a href="#" onclick="DialogBox_into(delete_dialog)" title="删除">删除</a></td>';
							str = str + '</tr>';
							$('#userList tbody').append(str);
						}

						var pagecount = count % 10 == 0 ? count / 10
								: parseInt(count / 10) + 1;

						$("#totalPage").html(pagecount);
						$("#currentPage").html(pageIndex);
					}
				})

		/*  $.ajax({  
		         type : 'GET',  
		         contentType : 'application/json',  
		         url : 'UserAction',  
		         data : jsonData,  
		         dataType : 'json',  
		         success : function(data) {  
		             var result = JSON.parse(data);  
		             if (result.result_code == 1){  
		                 $('#userList tbody tr').remove();//移除先前数据  
		             for ( var i = 0; i < result.data.length; i++) {  
		                 $('#userList tbody').append('<tr><td>'  
		                     + result.data[i].userName+ '</td><td>'  
		                     + result.data[i].email+ '</td><p>正常</p></td><td>'   
		                     + "<a href=\"javascript:JqueryDialog.Open('修改用户', 'UserAction?method=toUpdate&id="+result.data[i].userId+"&pageIndex="+result.page.pageIndex+"', 300, 300);\">编辑</a>|<a id='delete' href=\"javascript:deleteUser("+result.data[i].userId+","+result.page.pageIndex+")\">删除</a></td></tr>");  
		                     }  
		                 //显示分页  
		                 $('#userList tbody').append(showPage('showUserData',result.page));  
		             }  
		         },  
		             error : function(data) {  
		                 $('#userList tbody').append("获取数据失败！");  
		             }  
		         });   */
	}

	$(function() {
		showUserData(1);
	});
</script>
</head>

<body>
	<div id="header_zs"></div>
	<div id="header">
		<div id="header_3">
			<div id="header_1">
				<ul class="hd">
					<div class="logo pngfix">
						<img src="../images/logo.jpg" width="232" height="24" />
					</div>
				</ul>
			</div>
		</div>
		<div id="header_2">
			<div id="header_2_1">
				<img src="../images/ico01.jpg" width="26" height="26" />
			</div>
			<div id="header_2_2">
				<p><%=session.getAttribute("CURRENT_USER")%>
					&nbsp;&nbsp;&nbsp;&nbsp;<a href="logout">退出</a>
				</p>

			</div>
		</div>
	</div>
	<div id="content">
		<div id="content_left">
			<ul id="left_nav">
				<li><a href="guangliyuan_yonhu.html" class="curr"><span
						class="ico04"></span>用户</a></li>
				<li><a href="guangliyuan_juese.html"><span class="ico04">角色</span></a></li>
				<li><a href="guangliyuan_caidan.html"><span class="ico04">菜单</span></a></li>
			</ul>
		</div>
		<div id="content_right">
			<div id="right_nav">
				<div id="right_nav_1">
					<img src="../images/ico05.jpg" width="9" height="12" />
				</div>
				<div id="right_nav_2">
					你现在的位置：<a href="#">管理员</a> / 用户
				</div>
			</div>
			<div class="fg"></div>
			<div class="right_btn">
				<a href="guangliyuan_yonhu_chuangjian.html">创建用户 </a> <a href="#"
					onclick="DialogBox_into(delete_dialog)" title="删除">批量删除</a>
			</div>
			<!-- 删除弹出框-->
			<div id="delete_dialog" style="display: none;">
				<div class="mask_layer"></div>
				<div class="dialog_operate dialog_del">
					<div class="dialog_title">
						<label>删除</label>
					</div>
					<div class="dialog_content">
						<div style="font-weight: bold; line-height: 20px;">
							确认删除该数据吗？</div>
						<div style="line-height: 20px;">数据名称数据名称数据名称</div>
					</div>
					<div class="btn_group">
						<div class="cancel" onclick="DialogBox_exit(delete_dialog)">取消</div>
						<div class="ok"
							onclick="DialogBox_exit(delete_dialog);DialogBox_SuccOrFail_into(succ_dialog)">确定</div>
					</div>
				</div>
			</div>
			<!--操作成功提示框-->
			<div id="succ_dialog" style="display: none;">
				<!--遮罩层-->
				<!--<div class="mask_layer">
      </div>-->
				<!--显示内容层-->
				<div class="dialog_tip dialog_operate">
					<div class="dialog_tip_text">
						<h3 class="h3_success">操作成功</h3>
					</div>
				</div>
			</div>
			<!--操作失败提示框-->
			<div class="right_box">
				<div class="search">
					<input type="text" id="searchText" style="width: 180px"
						placeholder="请输入关键字" />
				</div>
				<div class="search_1">
					<a href="javascript:;"><img src="../images/button01_1.jpg"
						width="46" height="26" onclick="showUserData(1)" /></a>
				</div>
				<div class="list" style="margin-top: 18px;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"
						id="userList">
						<thead>
							<tr>
								<td width="10%" height="36" bgcolor="#f4f7f8"><strong><input
										type="checkbox" /></strong></td>
								<td width="20%" height="36" bgcolor="#f4f7f8"><strong>用户名</strong></td>
								<td height="30%" bgcolor="#f4f7f8"><strong>邮箱</strong></td>
								<td height="20%" bgcolor="#f4f7f8"><strong>状态</strong></td>
								<td width="20%" height="36" bgcolor="#f4f7f8"><strong>操作</strong></td>
							</tr>
						</thead>
						<tbody>
							<!-- <tr>
							<td height="36"><input type="checkbox" /></td>
							<td>UserName</td>
							<td>ccsuhuang@163.com</td>
							<td>正常</td>
							<td><a href="guangliyuan_yonhu_bianji.html" title="编辑">编辑</a>&nbsp;&nbsp;<a
								href="#" onclick="DialogBox_into(delete_dialog)" title="删除">删除</a></td>
						</tr> -->
						</tbody>
					</table>
				</div>
				<div class="fenye">
					<ul>
						<li><a href="javascript:showUserData('last')">尾页</a></li>
						<li><a href="javascript:showUserData('next')">下一页</a></li>
						<li><a href="javascript:showUserData('prev')">上一页</a></li>
						<li><a href="javascript:showUserData(1)">首页</a></li>
						<li>当前所在页：<span class="font_2" id="currentPage"></span></li>
						<li>总页数：<span id="totalPage"></span></li>
					</ul>
				</div>
			</div>
			<div id="failure_dialog" style="display: none;">
				<!--遮罩层-->
				<!--<div class="mask_layer">
      </div>-->
				<!--显示内容层-->
				<div class="dialog_tip  dialog_operate">
					<div class="dialog_tip_text">
						<h3 class="h3_failure">操作失败</h3>
					</div>
				</div>

			</div>
</body>
</html>
